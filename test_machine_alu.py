import contextlib
import io
import logging
import tempfile

import pytest

import machine
from isa import Opcode


@pytest.mark.golden_test("golden/unit/machine/*.yml")
def test_whole_by_golden(golden, caplog):
    # Установим уровень отладочного вывода на DEBUG
    caplog.set_level(logging.DEBUG)

    # Создаём временную папку для тестирования приложения.
    with tempfile.TemporaryDirectory():
        with contextlib.redirect_stdout(io.StringIO()):
            data_path = machine.DataPath(100, [])
            data_path.execute(Opcode.LD, int(golden["a"]), False)
            data_path.execute(golden["operation"].replace("\n", ""), int(golden["b"]), False)
            a = data_path.acc

        # Проверяем что ожидания соответствуют реальности.
        assert a == int(golden.out["result"])

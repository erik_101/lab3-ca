import json
from enum import Enum


class AddressingType(str, Enum):
    IMMEDIATE = "immediate"
    ABSOLUTE = "absolute"


parse_addressing = {
    "immediate": AddressingType.IMMEDIATE,
    "absolute": AddressingType.ABSOLUTE
}

INPUT_CELL = 998
OUTPUT_CELL = 999


class Opcode(str, Enum):
    """Opcode для ISA."""
    ADD = 'add'
    SUB = 'sub'
    MUL = 'mul'
    DIV = 'div'
    MOD = 'mod'
    LD = 'ld'
    STORE = 'store'
    CMP = 'cmp'
    BEQ = 'beq'
    BNE = 'bne'
    BLE = 'ble'
    BLT = 'blt'
    BGE = 'bge'
    BGT = 'bgt'
    JUMP = 'jump'
    HALT = 'halt'


calculable_opcodes = [Opcode.ADD, Opcode.SUB, Opcode.MUL, Opcode.DIV, Opcode.MOD]
comparable_opcodes = [Opcode.CMP]
transitional_opcodes = [Opcode.BEQ, Opcode.BNE, Opcode.BLE,
                        Opcode.BLT, Opcode.BGE, Opcode.BGT, Opcode.JUMP]
memory_opcodes = [Opcode.LD, Opcode.STORE]

name2opcode = {
    'add': Opcode.ADD,
    'sub': Opcode.SUB,
    'mul': Opcode.MUL,
    'div': Opcode.DIV,
    'mod': Opcode.MOD,
    'ld': Opcode.LD,
    'store': Opcode.STORE,
    'cmp': Opcode.CMP,
    'beq': Opcode.BEQ,
    'bne': Opcode.BNE,
    'ble': Opcode.BLE,
    'blt': Opcode.BLT,
    'bge': Opcode.BGE,
    'bgt': Opcode.BGT,
    'jump': Opcode.JUMP,
    'halt': Opcode.HALT,
}


def write_code(filename, code):
    """Записать машинный код в файл."""
    with open(filename, "w", encoding="utf-8") as file:
        file.write(json.dumps(code, indent=4))
        code.clear()


def read_code(filename):
    """Прочесть машинный код из файла."""
    with open(filename, encoding="utf-8") as file:
        code = json.loads(file.read())

    for instr in code:
        instr['opcode'] = Opcode(instr['opcode']).value

    return code

import logging
import sys
from enum import Enum

import isa
from isa import Opcode, calculable_opcodes, comparable_opcodes, \
    transitional_opcodes, memory_opcodes, read_code, \
    name2opcode, parse_addressing, AddressingType


class OutputMode(str, Enum):
    SYMBOL = 'symbol'
    NUMERIC = 'numeric'


class DataPath:
    def __init__(self, data_memory_size: int, input_buffer: list):
        assert data_memory_size > 0, "Data memory size should be non-zero"
        self.data_memory_size = data_memory_size
        self.data_memory = [0] * data_memory_size
        self.ar = 0
        self.acc = 0
        self.input_buffer = input_buffer
        self.output_buffer = []
        self.output_mode = OutputMode.NUMERIC

    def execute(self, sel_opcode: Opcode, arg: int = None, sel_data: bool = False):
        if sel_data:
            second_operand = self.data_memory[self.ar]
        else:
            second_operand = arg

        if sel_opcode == Opcode.LD:
            self.__alu(None, second_operand)

        elif sel_opcode in calculable_opcodes:
            self.__alu(sel_opcode, second_operand)

    def z_flag(self):
        return self.acc == 0

    def n_flag(self):
        return self.acc < 0

    def latch_mem(self):
        self.data_memory[self.ar] = self.acc

    def latch_ar(self, ar):
        assert ar < self.data_memory_size, "Not enough memory"
        self.ar = ar

    def __alu(self, opcode: Opcode, second_operand: int):
        def check_overflow(value: int):
            if value > 2 ** 31 - 1 or value < - 2 ** 31:
                return (2 ** 31 - 1) & value
            return value

        second_operand = check_overflow(second_operand)

        alu_out = 0
        if opcode is None:
            alu_out = second_operand
        elif opcode == Opcode.ADD:
            alu_out = self.acc + second_operand
        elif opcode == Opcode.SUB:
            alu_out = self.acc - second_operand
        elif opcode == Opcode.MUL:
            alu_out = self.acc * second_operand
        elif opcode == Opcode.DIV:
            alu_out = self.acc // second_operand
        elif opcode == Opcode.MOD:
            alu_out = self.acc % second_operand

        self.acc = int(check_overflow(alu_out))

    def input(self):
        if len(self.input_buffer) == 0:
            raise EOFError()
        symbol = self.input_buffer.pop(0)
        symbol_code = ord(symbol)
        assert -128 <= symbol_code <= 127, f"{{Input token is out of bound: {symbol_code}}}"
        self.data_memory[isa.INPUT_CELL] = symbol_code
        logging.debug('Input: %s', repr(symbol))

    def output(self):
        if self.output_mode == OutputMode.NUMERIC:
            symbol = self.data_memory[isa.OUTPUT_CELL]
        else:
            symbol = chr(self.data_memory[isa.OUTPUT_CELL])
        logging.info(f"{{New output symbol '{symbol}' will be added to "
                     f"{repr(''.join(str(self.output_buffer)))}}}")
        self.output_buffer.append(symbol)


class ControlUnit:
    def __init__(self, program, data_path):
        self.program = program
        self.ip = 0
        self.data_path = data_path
        self._tick = 0

    def tick(self):
        self._tick += 1

    def current_tick(self):
        return self._tick

    def latch_ip(self, sel_next: bool):
        if sel_next:
            self.ip += 1
        else:
            instr = self.program[self.ip]
            assert "arg" in instr, "Internal error!"
            self.ip = instr["arg"]

    def decode_and_execute_instruction(self):
        insrt = self.program[self.ip]
        opcode = name2opcode[insrt["opcode"]]
        addressing = insrt.get("addressing", None)
        arg = insrt.get("arg", None)
        sel_data = False

        if isinstance(arg, str):
            if arg[0] == "'" or arg[0] == '"':
                arg = ord(arg[1])
                self.data_path.output_mode = OutputMode.SYMBOL

        if opcode is Opcode.HALT:
            raise StopIteration()

        if opcode in transitional_opcodes:
            if opcode == Opcode.JUMP:
                self.latch_ip(sel_next=False)

            else:
                if not self.data_path.n_flag() and not self.data_path.z_flag():
                    if opcode in [Opcode.BNE, Opcode.BGE, Opcode.BGT]:
                        self.latch_ip(sel_next=False)
                    else:
                        self.latch_ip(sel_next=True)

                elif self.data_path.n_flag() and not self.data_path.z_flag():
                    if opcode in [Opcode.BNE, Opcode.BLE, Opcode.BLT]:
                        self.latch_ip(sel_next=False)
                    else:
                        self.latch_ip(sel_next=True)

                elif not self.data_path.n_flag() and self.data_path.z_flag():
                    if opcode in [Opcode.BEQ, Opcode.BLE, Opcode.BGE]:
                        self.latch_ip(sel_next=False)
                    else:
                        self.latch_ip(sel_next=True)

            self.tick()

        else:
            if addressing:
                if parse_addressing[addressing] == AddressingType.ABSOLUTE:
                    sel_data = True
                    self.data_path.latch_ar(arg)
                else:
                    sel_data = False

            if opcode in calculable_opcodes:
                self.data_path.execute(opcode, arg, sel_data)
                self.latch_ip(sel_next=True)
                self.tick()

            elif opcode in comparable_opcodes:
                self.data_path.execute(Opcode.SUB, arg, sel_data)

                self.latch_ip(sel_next=True)
                self.tick()

            elif opcode in memory_opcodes:
                if opcode == Opcode.LD:
                    if sel_data and arg == isa.INPUT_CELL:
                        self.data_path.output_mode = OutputMode.SYMBOL
                        self.data_path.input()
                    self.data_path.execute(opcode, arg, sel_data)
                else:
                    self.data_path.latch_mem()
                    if sel_data and arg == isa.OUTPUT_CELL:
                        self.data_path.output()

                self.latch_ip(sel_next=True)
                self.tick()

    def __repr__(self):
        state = "{{TICK: {}, IP: {}, AR: {}, OUT: {}, ACC: {}, N: {}, Z: {}}}".format(
            self._tick,
            self.ip,
            self.data_path.ar,
            self.data_path.data_memory[self.data_path.ar],
            self.data_path.acc,
            self.data_path.n_flag(),
            self.data_path.z_flag()
        )

        instr = self.program[self.ip]
        opcode = instr["opcode"]
        arg = instr.get("arg", "")
        addressing_type = instr.get("addressing", "")
        action = "{} {} {}".format(opcode, arg, addressing_type)

        return "{} {}".format(state, action)


def simulation(code, input_tokens, data_memory_size, limit):
    """Запуск симуляции процессора"""
    data_path = DataPath(data_memory_size, input_tokens)
    control_unit = ControlUnit(code, data_path)
    instruction_counter = 0

    logging.debug("%s", control_unit)
    try:
        while True:
            assert limit > instruction_counter, "Too long execution, increase limit!"
            control_unit.decode_and_execute_instruction()
            instruction_counter += 1
            logging.debug('%s', control_unit)
    except EOFError:
        logging.warning('Input buffer is empty!')
    except StopIteration:
        pass
    logging.info('Output buffer: %s', repr(''.join(str(data_path.output_buffer))))
    return ''.join(str(data_path.output_buffer)), instruction_counter, control_unit.current_tick()


def main(args):
    assert len(args) == 2, "Wrong arguments: machine.py <code_file> <input_file>"
    code_file, input_file = args

    code = read_code(code_file)
    with open(input_file, encoding="utf-8") as file:
        input_text = file.read()
        input_token = []
        for char in input_text:
            input_token.append(char)

    output, instruction_counter, ticks \
        = simulation(code, input_tokens=input_token, data_memory_size=1000, limit=1000)
    print(''.join(output))
    print("Instruction Counter:", instruction_counter, "Ticks:", ticks)


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv[1:])

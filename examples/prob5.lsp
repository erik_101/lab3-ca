(define r 1)
(define i 2)

(loop (<= i 20) (cond (/= (mod r i) 0) ((define k i) (loop (<= (* k i) 20) (setq k (* k i))) (setq r (* r k)))) (setq i (+ i 1)))

(print r)

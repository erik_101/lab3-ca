import sys

import isa
from isa import Opcode, AddressingType
from isa import write_code


def tokenize(chars: str) -> list:
    return chars.replace('(', ' ( ').replace(')', ' ) ').split()


def atom(token: str):
    try:
        return int(token)
    except ValueError:
        try:
            return float(token)
        except ValueError:
            return str(token)


def read_tokens(tokens: list):
    if len(tokens) == 0:
        raise SyntaxError('Unexpected EOF')
    token = tokens.pop(0)

    if token == '(':
        part = []
        while tokens[0] != ')':
            part.append(read_tokens(tokens))
        tokens.pop(0)
        return part

    if token == ')':
        raise SyntaxError('Unexpected )')

    return atom(token)


def parse(program: str):
    return read_tokens(tokenize(program))


data_mapping = {}
CELL_COUNT = 0

code = []


def create_opcode(opcode: Opcode, value: int = None, addressing_type: AddressingType = None):
    return {"opcode": opcode, "addressing": addressing_type, "arg": value}


def create_math_opcode(opcode: str, value: int, addressing_type: AddressingType):
    if opcode == "+":
        return create_opcode(Opcode.ADD, value, addressing_type)
    if opcode == "-":
        return create_opcode(Opcode.SUB, value, addressing_type)
    if opcode == "*":
        return create_opcode(Opcode.MUL, value, addressing_type)
    if opcode == "/":
        return create_opcode(Opcode.DIV, value, addressing_type)
    if opcode == "mod":
        return create_opcode(Opcode.MOD, value, addressing_type)
    raise SyntaxError("Unavailable operation")


def create_conditional_transition_opcode(opcode: str, value):
    """Создаем Opcod'ы обратных действий, так как для перехода должно выполниться условие"""
    if opcode == "<=":
        return create_opcode(Opcode.BGT, value, AddressingType.ABSOLUTE)
    if opcode == "<":
        return create_opcode(Opcode.BGE, value, AddressingType.ABSOLUTE)
    if opcode == ">=":
        return create_opcode(Opcode.BLT, value, AddressingType.ABSOLUTE)
    if opcode == ">":
        return create_opcode(Opcode.BLE, value, AddressingType.ABSOLUTE)
    if opcode == "=":
        return create_opcode(Opcode.BNE, value, AddressingType.ABSOLUTE)
    if opcode == "/=":
        return create_opcode(Opcode.BEQ, value, AddressingType.ABSOLUTE)
    raise SyntaxError("Unavailable operation")


def get_arg_and_addressing(arg):
    if isinstance(arg, str):
        if arg[0] == "'" or arg[0] == '"':
            assert len(arg) == 3, "It is not possible to declare a variable of several characters"
            arg2 = arg
            arg2_addressing_type = AddressingType.IMMEDIATE
        else:
            arg2 = data_mapping[arg]
            arg2_addressing_type = AddressingType.ABSOLUTE
    else:
        arg2 = arg
        arg2_addressing_type = AddressingType.IMMEDIATE
    return arg2, arg2_addressing_type


def translate(line: list):
    global CELL_COUNT

    if line[0] == "define":
        if isinstance(line[2], list):
            translate(line[2])
            data_mapping[line[1]] = CELL_COUNT
            code.append(create_opcode(Opcode.STORE, CELL_COUNT, AddressingType.ABSOLUTE))
        else:
            arg2, arg2_addressing_type = get_arg_and_addressing(line[2])

            data_mapping[line[1]] = CELL_COUNT

            code.append(create_opcode(Opcode.LD, arg2, arg2_addressing_type))
            code.append(create_opcode(Opcode.STORE, CELL_COUNT, AddressingType.ABSOLUTE))
        CELL_COUNT += 1

    elif line[0] == "setq":
        if isinstance(line[2], list):
            translate(line[2])
            code.append(create_opcode(Opcode.STORE, data_mapping[line[1]], AddressingType.ABSOLUTE))
        else:
            arg2, arg2_addressing_type = get_arg_and_addressing(line[2])

            code.append(create_opcode(Opcode.LD, arg2, arg2_addressing_type))
            code.append(create_opcode(Opcode.STORE, data_mapping[line[1]], AddressingType.ABSOLUTE))

    elif line[0] in ["+", "-", "*", "/", "mod"]:
        arg1, arg1_addressing_type = get_arg_and_addressing(line[1])
        arg2, arg2_addressing_type = get_arg_and_addressing(line[2])

        code.append(create_opcode(Opcode.LD, arg1, arg1_addressing_type))
        code.append(create_math_opcode(line[0], arg2, arg2_addressing_type))

    elif line[0] in ["<", ">", "<=", ">=", "=", "/="]:
        if isinstance(line[1], list):
            translate(line[1])
            arg2, arg2_addressing_type = get_arg_and_addressing(line[2])
            code.append(create_opcode(Opcode.CMP, arg2, arg2_addressing_type))
        else:
            arg1, arg1_addressing_type = get_arg_and_addressing(line[1])
            arg2, arg2_addressing_type = get_arg_and_addressing(line[2])

            code.append(create_opcode(Opcode.LD, arg1, arg1_addressing_type))
            code.append(create_opcode(Opcode.CMP, arg2, arg2_addressing_type))

    elif line[0] == "loop":
        condition = line[1]
        expressions = line[2:]

        begin_condition = len(code)
        translate(condition)
        begin_code = len(code)

        code.append(create_conditional_transition_opcode(condition[0], None))

        for expression in expressions:
            translate(expression)

        code[begin_code] = create_conditional_transition_opcode(condition[0], len(code) + 1)
        code.append(create_opcode(Opcode.JUMP, begin_condition, AddressingType.ABSOLUTE))

    elif line[0] == "cond":
        condition = line[1]
        then_expression = line[2]
        else_expression = None
        if len(line) == 4:
            else_expression = line[3]

        translate(condition)
        begin_code = len(code)

        code.append(create_conditional_transition_opcode(condition[0], None))

        for expression in then_expression:
            translate(expression)

        code[begin_code] = create_conditional_transition_opcode(condition[0], len(code))

        if else_expression:
            for expression in else_expression:
                translate(expression)

    elif line[0] == "read":
        code.append(create_opcode(Opcode.LD, isa.INPUT_CELL, AddressingType.ABSOLUTE))

    elif line[0] == "print":
        arg1, arg1_addressing_type = get_arg_and_addressing(line[1])

        code.append(create_opcode(Opcode.LD, arg1, arg1_addressing_type))
        code.append(create_opcode(Opcode.STORE, isa.OUTPUT_CELL, AddressingType.ABSOLUTE))


def main(args):
    global CELL_COUNT

    assert len(args) == 2, \
        "Wrong arguments: translator.py <input_file> <target_file>"
    source, target = args

    loc = 0

    with open(source, "rt", encoding="utf-8") as file:
        for line in file:
            if line != "\n":
                loc += 1
                translate(parse(line))

    code.append(create_opcode(Opcode.HALT))

    print("source LoC:", loc, "code instr:", len(code))
    write_code(target, code)

    data_mapping.clear()
    code.clear()
    CELL_COUNT = 0


if __name__ == "__main__":
    main(sys.argv[1:])

# Архитектура компьютера. Лабораторная работа 3

- Карапетян Эрик Акопович, гр. P33121
- `lisp | acc | harv | hw | instr | struct | stream | mem | prob5`


## Язык программирования

``` ebnf
<program>         ::= <S_expression>

<S_expression>    ::= "(" <Operation> ")"

<Operation>       ::= <A0>
                  |   <A1> " "+ <Argument>
                  |   <A2> " "+ <Argument> " "+ <Argument>
                  |   <A2+> " "+ <Argument> " "+ <Arguments>

<A0>              ::= "read"

<A1>              ::= "print"

<A2>              ::= "define" | "setq"
                  | "+" | "-" | "*" | "/" | "mod"
                  | "<" | ">" | "<=" | ">=" | "=" | "/="

<A2+>             ::= "loop" | "cond"

<Arguments>       ::= <Argument>
                  |   <Argument> <Arguments>

<Argument>        ::= <Atom>
                  |   <S_expression>

<Atom>            ::= <symbol>
                  |   <number>
                  |   <char>

<symbol>          ::= [a-zA-Z]+

<number>          ::= [-2^31; 2^31 - 1]

<char>            ::= "'" <any UTF-8 symbol> "'"
                  |   '"' <any UTF-8 symbol> '"'
```

Операции могут не иметь аргумента, иметь один или два аргумента, а также более двух аргументов.
Поддерживаемые аргументы:
- атомарные:
  - symbol - имя переменной
  - char - символьное значение переменной
  - number - числовое значение переменной
- выражение - результат сначала вычисляется, а потом уже выполняются последующие действия над ним

Операции:
- `define <name> <arg>` -- объявление переменной с именем `name` со значением `arg`
- `setq <name> <arg>` -- переопределение значения переменной с именем `name` на значение `arg`
- `+ <arg1> <arg2>` -- функция прибавления `arg2` к `arg1` с возвратом результата
- `- <arg1> <arg2>` -- функция вычитания `arg2` из `arg1` с возвратом результата
- `* <arg1> <arg2>` -- умножение `arg2` с `arg1` с возвратом результата
- `/ <arg1> <arg2>` -- целочисленное деление `arg1` на `arg2` с возвратом результата
- `< <arg1> <arg2>` -- булевый результат сравнения `arg1` `<` `arg2`
- `> <arg1> <arg2>` -- булевый результат сравнения `arg1` `>` `arg2`
- `<= <arg1> <arg2>` -- булевый результат сравнения `arg1` `<=` `arg2`
- `>= <arg1> <arg2>` -- булевый результат сравнения `arg1` `>=` `arg2`
- `= <arg1> <arg2>` -- булевый результат сравнения `arg1` `=` `arg2`
- `/= <arg1> <arg2>` -- булевый результат сравнения `arg1` `/=` `arg2`
- `loop <condition> <operations>` -- цикл, в котором проверяется `condition`, если оно истинно, выполняются `operations`, если нет, происходит переход к следующей операции
                                      после выполнения всех операций, снова проверяется условие, снова выполняются `operations` и т д
- `cond <condition> <operations>` -- проверяется `condition`, если оно истинно, выполняются `operations`, если нет, происходит переход к следующей операции
- `read` -- чтение символа из стандартного потока ввода
- `print <arg>` -- печать `arg` в стандартный поток вывода


## Организация памяти

- Есть возможность объявления переменных при помощи операции `define`
- Есть возможность переопределения значения переменных при помощи операции `setq`

Модель памяти процессора:

- Память команд. Машинное слово -- не определено. Реализуется списком словарей, описывающих инструкции (одно слово -- одна ячейка).
- Память данных. Машинное слово -- 32 бит, знаковое. Реализуется списком чисел.

Типы адресации:

- Прямая регистровая: операндом инструкции является регистр.
- Непосредственная загрузка: операндом является константа, подаваемая как один из аргументов.


## Система команд

Особенности процессора:

- Машинное слово -- 32 бита, знаковое.
- Память данных:
    - доступ к памяти данных только через команды `ld` и `store`;
    - на шину данных для записи подается результат с выхода АЛУ;
    - на шину адреса подается адрес, который выставляет Control Unit;
- АЛУ:
    - на правый вход АЛУ вместо значения может быть подана константа из инструкции;
    - АЛУ поддерживает операции: `add`, `sub`, `mul`, `div`, `mod`, вывод правого входа;
- Регистры:
    - регистр аккумулятора `acc`:
      - построена система команд вокруг него 
      - используется для определения флагов N и Z
    - регистр счетчика команд `ip`:
      - используется для определения инструкции для выполнения
      - стандартное поведение: увеличение на 1 после каждой инструкции
      - можно манипулировать с помощью команд перехода
    - регистр адреса `ar`:
      - используется для адресации данных в памяти
      - необходим для абсолютной адресации
- Ввод-вывод:
    - memory-mapped через 2 ячейки в памяти данных: input_cell, output_cell

    
### Набор инструкций

Набор инструкций создается при транслировании языка Lisp.

- `add <arg>` -- 1 такт
- `sub <arg>` -- 1 такт
- `mul <arg>` -- 1 такт
- `div <arg>` -- 1 такт
- `mod <arg>` -- 1 такт
- `ld <arg>` -- 1 такт
- `store <arg>` -- 1 такт
- `cmp <arg>` -- 1 такт
- `beq <arg>` -- 1 такт
- `bne <arg>` -- 1 такт
- `ble <arg>` -- 1 такт
- `blt <arg>` -- 1 такт
- `bge <arg>` -- 1 такт
- `bgt <arg>` -- 1 такт
- `jump <arg>` -- 1 такт
- `halt` -- 0 тактов


### Кодирование инструкций

- Код Lisp сериализуется в инструкции из памяти команд, которые записаны в словарь JSON
```{
    [
        {
            "opcode": "ld",
            "addressing": "immediate",
            "arg": 1
        },
        {
            "opcode": "store",
            "addressing": "absolute",
            "arg": 0
        },
        {
            "opcode": "ld",
            "addressing": "immediate",
            "arg": 2
        },
        {
            "opcode": "store",
            "addressing": "absolute",
            "arg": 1
        },
        ...
    ]
}
```

где:

- opcode -- строка с кодом операции;
- addressing (optional) -- тип адресации;
- arg (optional) -- аргумент команды;

Типы данных в модуле isa, где:
- Opcode -- перечисление кодов операций;
- AddressingType -- перечисление типов адресации;


## Транслятор

Интерфейс командной строки: `translator.py <input_file> <target_file>`

Реализовано в модуле: [translator](./translator.py)

Этапы трансляции (функция `translate`):

1. Токенизация текста.
2. Формирование AST.
3. Генерация машинного кода.

Правила генерации машинного кода:

- одна инструкция процессора -- одна инструкция в коде;
- в конце генерации автоматически добавляется инструкция `halt`

## Модель процессора

Реализовано в модуле: [machine](./machine.py).

### Схема DataPath и ControlUnit

![https://gitlab.se.ifmo.ru/erik_101/lab3-ca/images/scheme.png](/images/scheme.png "Схема DataPath и ControlUnit") 

## DataPath

Реализован в классе `DataPath`.

- `data_memory` -- однопортовая, поэтому либо читаем, либо пишем.
- `input_cell` -- ячейка в памяти, соединенная со стандартным потоком ввода.
- `output_cell` -- ячейка в памяти, соединенная со стандартным потоком вывода.
- `input` -- вызовет остановку процесса моделирования, если буфер входных значений закончился. 
- `alu` -- АЛУ, выполняющее арифметические операции.
- `input_buffer` -- буфер со входными данными (стандартный поток ввода).
- `output_buffer` -- буфер с данными для записи (стандартный поток вывода).

Сигналы:

- `execute` -- рассчитать выходное значение АЛУ, подав на него сигнал с операцией и операндом в случае непосредственной загрузки.
- `latch_mem` -- защелкнуть в память значение аккумулятора.
- `latch_ar` -- защелкнуть в AR адрес операнда.
- `input` -- защелкнуть в `input_cell` данные с из `input_buffer`.
- `output` -- записать в `output_buffer` данные из `output_cell`.

Флаги:

- `zero` -- отражает наличие нулевого значения на выходе АЛУ. Используется для условных переходов.
- `negative` -- отражает наличие отрицательного значения на выходе АЛУ. Используется для условных переходов.

## ControlUnit
Реализован в классе `ControlUnit`.

- Hardwired (реализовано полностью на python).
- Моделирование на уровне инструкций.
- Трансляция инструкции в последовательность сигналов: `decode_and_execute_instruction`.
- `ip` - счетчик команд.

Сигнал:

- `latch_ip` -- сигнал для обновления счётчика команд в ControlUnit.

Особенности работы модели:

- Для журнала состояний процессора используется стандартный модуль logging.
- Количество инструкций для моделирования ограничено hardcoded константой.
- Остановка моделирования осуществляется при помощи исключений:
    - `EOFError` -- если нет данных для чтения из порта ввода-вывода;
    - `StopIteration` -- если выполнена инструкция `exit`.
- Управление симуляцией реализовано в функции `simulation`.


## Апробация

В качестве тестов использовано четыре алгоритма:

1. [hello world](examples/hello.lsp).
2. [cat](examples/cat.lsp) -- программа `cat`, повторяем ввод на выводе.
3. [prob5](examples/prob5.lsp) -- рассчитать НОК всех цифр от 1 до 20.

Юнит-тесты реализованы тут:
[translator_test](test_translate.py)
[test_machine_alu](test_machine_alu.py)

Интеграционные тесты реализованы тут:
[integration_test](integration_test.py)

Тестирование через golden test:
- Конфигурация юнит-тестов лежит в папке [golden/unit](golden/unit)
- Конфигурация интеграционных тестов лежит в папке [golden/integration](golden/integration)

CI:
```yaml
lab3-example:
  stage: test
  image:
    name: python-tools
    entrypoint: [""]
  script:
    - pip install pytest-golden
    - python3-coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t python3-coverage report
    - find . -type f -name "*.py" | xargs -t pep8 --ignore=E501
    - find . -type f -name "*.py" | xargs -t pylint --disable C0114,C0116,W0603,R0912,R0915,C0115,R0801,C0103,R0902,W1203,C0209
```
где:

- `python3-coverage` -- формирование отчёта об уровне покрытия исходного кода.
- `pytest` -- утилита для запуска тестов.
- `pep8` -- утилита для проверки форматирования кода. `E501` (длина строк) отключено.
- `pylint` -- утилита для проверки качества кода. Некоторые правила отключены в отдельных модулях с целью упрощения кода.
- Docker image `python-tools` включает в себя все перечисленные утилиты. Его конфигурация: [Dockerfile](./Dockerfile).

Пример использования и журнал работы процессора на примере `cat`:

```
[
    {
        "opcode": "ld",
        "addressing": "immediate",
        "arg": 0
    },
    {
        "opcode": "store",
        "addressing": "absolute",
        "arg": 0
    },
    {
        "opcode": "ld",
        "addressing": "absolute",
        "arg": 0
    },
    {
        "opcode": "cmp",
        "addressing": "immediate",
        "arg": 3
    },
    {
        "opcode": "bge",
        "addressing": "absolute",
        "arg": 13
    },
    {
        "opcode": "ld",
        "addressing": "absolute",
        "arg": 998
    },
    {
        "opcode": "store",
        "addressing": "absolute",
        "arg": 1
    },
    {
        "opcode": "ld",
        "addressing": "absolute",
        "arg": 1
    },
    {
        "opcode": "store",
        "addressing": "absolute",
        "arg": 999
    },
    {
        "opcode": "ld",
        "addressing": "absolute",
        "arg": 0
    },
    {
        "opcode": "add",
        "addressing": "immediate",
        "arg": 1
    },
    {
        "opcode": "store",
        "addressing": "absolute",
        "arg": 0
    },
    {
        "opcode": "jump",
        "addressing": "absolute",
        "arg": 2
    },
    {
        "opcode": "halt",
        "addressing": null,
        "arg": null
    }
]


DEBUG:root:{TICK: 0, IP: 0, AR: 0, OUT: 0, ACC: 0, N: False, Z: True} ld 0 immediate
DEBUG:root:{TICK: 1, IP: 1, AR: 0, OUT: 0, ACC: 0, N: False, Z: True} store 0 absolute
DEBUG:root:{TICK: 2, IP: 2, AR: 0, OUT: 0, ACC: 0, N: False, Z: True} ld 0 absolute
DEBUG:root:{TICK: 3, IP: 3, AR: 0, OUT: 0, ACC: 0, N: False, Z: True} cmp 3 immediate
DEBUG:root:{TICK: 4, IP: 4, AR: 0, OUT: 0, ACC: -3, N: True, Z: False} bge 13 absolute
DEBUG:root:{TICK: 5, IP: 5, AR: 0, OUT: 0, ACC: -3, N: True, Z: False} ld 998 absolute
DEBUG:root:Input: 'f'
DEBUG:root:{TICK: 6, IP: 6, AR: 998, OUT: 102, ACC: 102, N: False, Z: False} store 1 absolute
DEBUG:root:{TICK: 7, IP: 7, AR: 1, OUT: 102, ACC: 102, N: False, Z: False} ld 1 absolute
DEBUG:root:{TICK: 8, IP: 8, AR: 1, OUT: 102, ACC: 102, N: False, Z: False} store 999 absolute
INFO:root:{New output symbol 'f' will be added to '[]'}
DEBUG:root:{TICK: 9, IP: 9, AR: 999, OUT: 102, ACC: 102, N: False, Z: False} ld 0 absolute
DEBUG:root:{TICK: 10, IP: 10, AR: 0, OUT: 0, ACC: 0, N: False, Z: True} add 1 immediate
DEBUG:root:{TICK: 11, IP: 11, AR: 0, OUT: 0, ACC: 1, N: False, Z: False} store 0 absolute
DEBUG:root:{TICK: 12, IP: 12, AR: 0, OUT: 1, ACC: 1, N: False, Z: False} jump 2 absolute
DEBUG:root:{TICK: 13, IP: 2, AR: 0, OUT: 1, ACC: 1, N: False, Z: False} ld 0 absolute
DEBUG:root:{TICK: 14, IP: 3, AR: 0, OUT: 1, ACC: 1, N: False, Z: False} cmp 3 immediate
DEBUG:root:{TICK: 15, IP: 4, AR: 0, OUT: 1, ACC: -2, N: True, Z: False} bge 13 absolute
DEBUG:root:{TICK: 16, IP: 5, AR: 0, OUT: 1, ACC: -2, N: True, Z: False} ld 998 absolute
DEBUG:root:Input: 'o'
DEBUG:root:{TICK: 17, IP: 6, AR: 998, OUT: 111, ACC: 111, N: False, Z: False} store 1 absolute
DEBUG:root:{TICK: 18, IP: 7, AR: 1, OUT: 111, ACC: 111, N: False, Z: False} ld 1 absolute
DEBUG:root:{TICK: 19, IP: 8, AR: 1, OUT: 111, ACC: 111, N: False, Z: False} store 999 absolute
INFO:root:{New output symbol 'o' will be added to "['f']"}
DEBUG:root:{TICK: 20, IP: 9, AR: 999, OUT: 111, ACC: 111, N: False, Z: False} ld 0 absolute
DEBUG:root:{TICK: 21, IP: 10, AR: 0, OUT: 1, ACC: 1, N: False, Z: False} add 1 immediate
DEBUG:root:{TICK: 22, IP: 11, AR: 0, OUT: 1, ACC: 2, N: False, Z: False} store 0 absolute
DEBUG:root:{TICK: 23, IP: 12, AR: 0, OUT: 2, ACC: 2, N: False, Z: False} jump 2 absolute
DEBUG:root:{TICK: 24, IP: 2, AR: 0, OUT: 2, ACC: 2, N: False, Z: False} ld 0 absolute
DEBUG:root:{TICK: 25, IP: 3, AR: 0, OUT: 2, ACC: 2, N: False, Z: False} cmp 3 immediate
DEBUG:root:{TICK: 26, IP: 4, AR: 0, OUT: 2, ACC: -1, N: True, Z: False} bge 13 absolute
DEBUG:root:{TICK: 27, IP: 5, AR: 0, OUT: 2, ACC: -1, N: True, Z: False} ld 998 absolute
DEBUG:root:Input: 'o'
DEBUG:root:{TICK: 28, IP: 6, AR: 998, OUT: 111, ACC: 111, N: False, Z: False} store 1 absolute
DEBUG:root:{TICK: 29, IP: 7, AR: 1, OUT: 111, ACC: 111, N: False, Z: False} ld 1 absolute
DEBUG:root:{TICK: 30, IP: 8, AR: 1, OUT: 111, ACC: 111, N: False, Z: False} store 999 absolute
INFO:root:{New output symbol 'o' will be added to "['f', 'o']"}
DEBUG:root:{TICK: 31, IP: 9, AR: 999, OUT: 111, ACC: 111, N: False, Z: False} ld 0 absolute
DEBUG:root:{TICK: 32, IP: 10, AR: 0, OUT: 2, ACC: 2, N: False, Z: False} add 1 immediate
DEBUG:root:{TICK: 33, IP: 11, AR: 0, OUT: 2, ACC: 3, N: False, Z: False} store 0 absolute
DEBUG:root:{TICK: 34, IP: 12, AR: 0, OUT: 3, ACC: 3, N: False, Z: False} jump 2 absolute
DEBUG:root:{TICK: 35, IP: 2, AR: 0, OUT: 3, ACC: 3, N: False, Z: False} ld 0 absolute
DEBUG:root:{TICK: 36, IP: 3, AR: 0, OUT: 3, ACC: 3, N: False, Z: False} cmp 3 immediate
DEBUG:root:{TICK: 37, IP: 4, AR: 0, OUT: 3, ACC: 0, N: False, Z: True} bge 13 absolute
DEBUG:root:{TICK: 38, IP: 13, AR: 0, OUT: 3, ACC: 0, N: False, Z: True} halt None None
INFO:root:Output buffer: "['f', 'o', 'o']"
['f', 'o', 'o']
Instruction Counter: 38 Ticks: 38
```

| ФИО              | алг.  | LoC | code байт | code инстр. | инстр. | такт. | вариант |
|------------------|-------|-----|-----------|-------------|--------|-------|---------|
| Карапетян Э. А.  | hello | 12  | -         | 25          | 24     | 24    | -       |
| Карапетян Э. А.  | cat   | 2   | -         | 14          | 38     | 38    | -       |
| Карапетян Э. А.  | prob5 | 4   | -         | 31          | 322    | 322   | -       |

